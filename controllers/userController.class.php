<?php

/**
 * Description of index
 *
 * @author TOSHIBA
 */

class userController extends Controller {
    
    function __construct(){
        parent::__construct();
        Session::init();
        //set url for need
        $request = new Request();
        $this->view->url = $request->getURI();
    }
    
    function login() {
        if(Session::hasUser())
            header ('location: ./chatRoom');
        
        $request = new Request();
        $this->view->user = new UserModel();
        $this->view->messages='';
        
        if($request->isMethodePost())
        {
            //load the user from the base
            $this->view->user->loadByEmailAndPass($request->getPostParameter('username'),$request->getPostParameter('password'));
            if(!$this->view->user->getId()){
                $this->view->messages='Username/Password is unvalid!';
                echo json_encode(array('error'=>$this->view->messages));
                return ;
            }
            Session::setUser($this->view->user);
            echo json_encode(array('success'=>'OK','link'=>'user/chatRoom'));
            return ;
        }
        
        return $this->view->render('User/login');
    }
    
    function register() {
        if(Session::hasUser())
            header ('location: ./chatRoom');
        
        $request = new Request();
        $this->view->user = new UserModel();
        $this->view->messages='';
        if(!$request->isMethodePost() || !$request->getPostParameters())
            return $this->view->render('User/register');

        try{
            //create user
            $this->view->user->createFromData($request->getPostParameters())->save();
            $this->view->user->connected();
            Session::setUser($this->view->user);

            echo json_encode(array('success'=>'OK','link'=>'user/chatRoom'));
            return;
        }
        catch(Exception $ex)
        {
            $this->view->messages=$ex->getMessage();
            echo json_encode(array('error'=> $this->view->messages));
            return;
        }
    }
    
    function logout() {
        Session::getUser()->desconnected();
        Session::destroy();
        $this->view->messages='';
        header ('location: ./login');
    }
    
    function chatRoom() {
        if(!Session::hasUser())
            header ('location: ./login');
        
        $request = new Request();
        $this->view->messages='';
        
        $this->view->user = Session::getUser();
        $this->view->user->connected();
        //initialize chat room 
        $this->view->listUsers = $this->view->user->getListOfUsers();
        $this->view->messages = MessagesModel::getAllMessagesWithUsers();
        
        return $this->view->render('User/chatRoom');
    }
    
    function getConnectedUsers()
    {
        $request = new Request();
        $this->view->user = new UserModel();
        $this->view->messages='';
        
        if($request->isMethodePost())
        {
            try{
                //load the user from the base
                $users = UserModel::getUsersConnected();
                echo json_encode($users);
            }
            catch(Exception $ex)
            {
                echo $ex->getMessage();
            }
        }
    }
}
