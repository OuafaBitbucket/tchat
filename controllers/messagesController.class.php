<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of index
 *
 * @author TOSHIBA
 */
//require_once 'libs/Controller.class.php';
//require_once "models/TestModel.class.php";

class messagesController extends Controller {
    
    function __construct(){
        parent::__construct();
        Session::init();
        //set url for need
        $request = new Request();
        $this->view->url = $request->getURI();
    }
    
    function sendMessage()
    {
        $request = new Request();
        $this->view->message = new MessagesModel();
        $this->view->messages='';
        
        if($request->isMethodePost())
        {
            try{
                //load the user from the base
                $this->view->message->createFromData($request->getPostParameters())->insert();
                echo json_encode($this->view->message->toArrayForChat());
            }
            catch(Exception $ex)
            {
                echo $ex->getMessage();
            }
        }
    }
    
    function getLastMessages()
    {
        $request = new Request();
        $this->view->message = new MessagesModel();
        $this->view->messages='';
        if($request->isMethodePost())
        {
            try{
                //get last messages
                $messages = MessagesModel::getLastMessages($request->getPostParameter('last_message_id'));
                
                echo json_encode($messages);
            }
            catch(Exception $ex)
            {
                $this->view->messages=$ex->getMessage();
            }
        }
    }
}
