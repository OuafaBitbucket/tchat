SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Structure de la table `t_user`
--

CREATE TABLE IF NOT EXISTS `t_user` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gender` enum('Mr','Ms','Mrs') COLLATE utf8_bin DEFAULT NULL,
  `pseudo` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `firstname` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_bin NOT NULL,
  `username` varchar(32) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `phone` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `mobile` varchar(20) COLLATE utf8_bin NOT NULL DEFAULT '',
  `birthday` date DEFAULT NULL,
  `last_password_gen` timestamp NULL DEFAULT NULL,
  `lastlogin` timestamp NULL DEFAULT NULL,
  `lasttime` timestamp NULL DEFAULT NULL,
  `is_active` enum('YES','NO') COLLATE utf8_bin NOT NULL DEFAULT 'NO', 
  `is_connected` enum('NO','YES') COLLATE utf8_bin NOT NULL DEFAULT 'NO',
  `status` enum('ACTIVE','DELETE') COLLATE utf8_bin NOT NULL DEFAULT 'ACTIVE',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Structure de la table `t_chat_messages`
--
CREATE TABLE IF NOT EXISTS `t_chat_messages` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `source_user_id` int(11) unsigned NOT NULL ,
    `file` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
    `message` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `is_seen` enum('YES','NO') COLLATE utf8_bin NOT NULL DEFAULT 'NO',
    `seen_at` timestamp NULL DEFAULT NULL,
    `created_at` timestamp  NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8  AUTO_INCREMENT=1;

-- insert results
INSERT INTO `t_user` (`id`,`firstname`,`lastname`,`email`, `username`, `password`, `gender`) VALUES
(1, 'ouafa', 'ouafa', 'ouafa@mail.com', 'ouafa', '202cb962ac59075b964b07152d234b70', 'Ms'),
(2, 'user1', 'user1', 'user1@mail.com', 'user1', '202cb962ac59075b964b07152d234b70', 'Mr'),
(3, 'user2', 'user2', 'user2@mail.com', 'user2', '202cb962ac59075b964b07152d234b70', 'Mrs');

COMMIT;