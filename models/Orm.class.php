<?php

class Orm
{
    private static $DATABASE;

    protected static function getDatabase()
    {
        try {
            return self::$DATABASE = Database::getInstance();
        }
        catch (\Exception $e){
            header('location: '.dirname(__FILE__).'/install.php');
        }
    }

    public static function getAll($table)
    {
        self::getDatabase();
        $obj = get_called_class();
        $collection = array();
        $cnx = self::$DATABASE->prepare('SELECT * FROM ' . $table . ' ORDER BY id asc');
        $cnx->execute();
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $object = new $obj();
            $object->fillFromData($data);
            $collection[] = $object;
        }
        $cnx->closeCursor();
        return $collection;
    }

    public static function findBy($parameters=array(),$table){
        self::getDatabase();
        $obj = get_called_class();
        $collection = array();
        $query = "";
        foreach ($parameters as $key=>$value) {
            $value = self::$DATABASE->quote($value);
            $query .= " AND $key = $value";
        }
        $cnx = self::$DATABASE->prepare("SELECT * FROM ".$table." WHERE $query");
        $cnx->execute($parameters);
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $object = new $obj();
            $object->fillFromData($data);
            $collection[] = $object;
        }
        $cnx->closeCursor();
        return $collection;
    }

    public function findOneBy($parameters=array()){
        self::getDatabase();
        $query = "";
        foreach ($parameters as $key=>$value) {
            $value = self::$DATABASE->quote($value);
            $query .= " AND $key = $value";
        }
        $cnx = self::$DATABASE->prepare("SELECT * FROM ".$this->getTable()." WHERE 1=1 $query limit 1");
        $cnx->execute($parameters);
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $this->fillFromData($data);
        }
        $cnx->closeCursor();
        return $this;
    }

    public function update($parameters=array()){
        self::getDatabase();
        $query = "SET ";
        $i=0;
        foreach ($parameters as $value) {
            $query .= "  ".$value." = ".self::$DATABASE->quote($this->$value);
            if($i !== count($parameters)-1) $query .= ",";
            $i++;
        }
        $cnx = self::$DATABASE->prepare("UPDATE ".$this->getTable()." $query WHERE id=:id");
        $cnx->execute(array(':id'=> $this->id));
        return true;
    }

    public function insert(){
        try {
            self::getDatabase();
            $parameters = array();
            $values = array();
            foreach ($this as $key => $value) {
                if (!in_array($key,array('id','_source_user_id','table'))) {
                    $parameters[] = $key;
                    $values[$key] = self::$DATABASE->quote($value);
                }
            }
            $query = "INSERT INTO ".$this->getTable()."(" . implode(',', $parameters) . ") values (" . implode(',', $values) . ")";
            $cnx = self::$DATABASE->prepare($query);
            $cnx->execute();
            
            $this->id = self::$DATABASE->lastInsertId();
            return $this;
        }
        catch (\PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function delete(){
        try {
            self::getDatabase();
            $query = "DELETE FROM ".$this->getTable()." WHERE id=:id;";
            $cnx = self::$DATABASE->prepare($query);
            $cnx->execute(array(":id"=> $this->id));
            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function fillFromData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->$key=$value;
        }
    }
}