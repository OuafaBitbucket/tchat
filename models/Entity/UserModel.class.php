<?php

class UserModel extends Orm
{
    protected $id,$gender,$pseudo,$firstname,$lastname,$email,$phone,$mobile,$birthday=null,$password,
              $last_password_gen=null,$lastlogin=null,$avatar,$username,
              $lasttime=null,$url,$is_connected,$created_at,$updated_at=null,$is_active,$status;    
    
    const table="t_user";
    protected $table="t_user";
            
    function __construct() { 
        $this->getDefaults();
    }
    
    function getTable()
    {
        return $this->table;
    }
    
    function getId() {
        return $this->id;
    }

    function getPseudo() {
        return $this->pseudo;
    }

    function getPhone() {
        return $this->phone;
    }

    function getMobile() {
        return $this->mobile;
    }

    function getBirthday() {
        return $this->birthday;
    }

    function getLast_password_gen() {
        return $this->last_password_gen;
    }

    function getLastlogin() {
        return $this->lastlogin;
    }

    function getAvatar() {
        return $this->avatar;
    }

    function getCulture() {
        return $this->culture;
    }

    function getUsername() {
        return $this->username;
    }

    function getLasttime() {
        return $this->lasttime;
    }

    function getUrl() {
        return $this->url;
    }

    function getIs_connected() {
        return $this->is_connected;
    }

    function getCreated_at() {
        return $this->created_at;
    }

    function getUpdated_at() {
        return $this->updated_at;
    }

    function getIs_active() {
        return $this->is_active;
    }

    function getStatus() {
        return $this->status;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setPseudo($pseudo) {
        $this->pseudo = $pseudo;
    }

    function setPhone($phone) {
        $this->phone = $phone;
    }

    function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    function setBirthday($birthday) {
        $this->birthday = $birthday;
    }

    function setLast_password_gen($last_password_gen) {
        $this->last_password_gen = $last_password_gen;
    }

    function setLastlogin($lastlogin) {
        $this->lastlogin = $lastlogin;
    }

    function setAvatar($avatar) {
        $this->avatar = $avatar;
    }

    function setCulture($culture) {
        $this->culture = $culture;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setLasttime($lasttime) {
        $this->lasttime = $lasttime;
    }

    function setUrl($url) {
        $this->url = $url;
    }

    function setIsConnected($is_connected) {
        $this->is_connected = $is_connected;
    }

    function setCreated_at($created_at) {
        $this->created_at = $created_at;
    }

    function setUpdated_at($updated_at) {
        $this->updated_at = $updated_at;
    }

    function setIs_active($is_active) {
        $this->is_active = $is_active;
    }

    function setStatus($status) {
        $this->status = $status;
    }
     
    function connected()
    {
        $this->setIsConnected('YES');
        try {
            $this->update(array('is_connected'));
        }
        catch(PDOException $ex)
        {
            throw new Exception($ex->getMessage());
        }
    }
     
    function desconnected()
    {
        $this->setIsConnected('NO');
        try {
            $this->update(array('is_connected'));
        }
        catch(PDOException $ex)
        {
            throw new Exception($ex->getMessage());
        }
    }
            
    function loadByEmailAndPass($email,$pass)
    {
        $this->email=$email;
        $this->password= md5($pass);
        
        $cnx = self::getDatabase()->prepare("SELECT * FROM ".self::table." WHERE (email=:email OR username=:email) AND password=:pass;");
        $cnx->execute(array(':email'=> $this->getEmail(),':pass'=> $this->getPassword()));
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $this->fillFromData($data);
        }
        $cnx->closeCursor();
        return $this;
    }
    
    function getListOfUsers()
    {        
        $collection = array();
        $cnx = self::getDatabase()->prepare("SELECT * FROM ".self::table." WHERE id!=:id;");
        $cnx->execute(array(':id'=> $this->getId()));
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $user = new self();
            $user->fillFromData($data);
            $collection[] = $user;
        }
        $cnx->closeCursor();
        return $collection;
    }
    
    protected function getDefaults()
    {
       $this->created_at=isset($this->created_at)?$this->created_at:date("Y-m-d H:i:s");
       $this->updated_at=isset($this->updated_at)?$this->updated_at:date("Y-m-d H:i:s");     
       $this->birthday=isset($this->birthday)?$this->birthday:date("Y-m-d H:i:s");     
       $this->gender=isset($this->gender)?$this->gender:"Mr"; 
       $this->status=isset($this->status)?$this->status:"ACTIVE";       
       $this->is_active=isset($this->is_active)?$this->is_active:"NO"; 
       $this->is_connected=isset($this->is_connected)?$this->is_connected:"NO"; 
       $this->last_password_gen=isset($this->last_password_gen)?$this->last_password_gen:date("Y-m-d H:i:s"); 
       $this->lastlogin=isset($this->lastlogin)?$this->lastlogin:date("Y-m-d H:i:s"); 
       $this->lasttime=isset($this->lasttime)?$this->lasttime:date("Y-m-d H:i:s"); 
    }
    
    function getValuesForUpdate()
    {
        $this->set('updated_at',date("Y-m-d H:i:s"));   
    }
    
    public function isExistQuery($db)    
    {
        //email
    }
        
    /* =================================== P I C T U R E S =========================================== */
    
    public function hasAvatar()
    {
        return (boolean)$this->avatar;
    }
    
    public function deleteAvatar()
    {
        //delete avatar
        return $this;
    }       
 
    static function getAvatarName()
    {
       return 'avatar';
    } 
    
    function setAvatarFile($file_extension)
    {      
        $this->avatar= self::getAvatarName().".".$file_extension;
        return $this;
    }
    
    function getLastName($ucfirst=false)
    {
        if ($ucfirst)
            return ucfirst($this->lastname);
        return $this->lastname;
    }
    
    function getFirstName($ucfirst=false)
    {
        if ($ucfirst)
            return ucfirst($this->lastname);
        return $this->firstname;
    }
    
    function getEmail()
    {
        return $this->email;
    }
    
    function getPassword()
    {
        return $this->password;
    }             
    
    function getGender()
    {
        return $this->gender;
    }
    
    function getFullName($upper=true)
    {
        if ($upper)
            return $this->gender.". ".strtoupper($this->__toString());
        return $this->gender.". ".$this->__toString();
    }
    
    // FOR DISPLAY
    public function __toString()
    {      
       return (string) $this->firstname.' '.$this->lastname;
    }
    
    public function getName($ucfirst=true)
    {      
        if ($ucfirst)
            return ucfirst($this->firstname)." ".ucfirst($this->lastname);
        return $this->__toString();  
    }
    function enable()
    {
        $this->is_active='YES';
        return $this;
    }
    
    function disble()
    {
        $this->is_active='NO';
        return $this;
    }
    
    function activate()
    {
        $this->status='ACTIVE';
        return $this;
    }
    
    function disactivate()
    {
        $this->status='DELETE';
        return $this;
    }   
  
    function encryptPassword()
    {                  
        $this->password=md5($this->password);    
        return $this;
    }
    
    function getSignature()
    {
        $str="";
        foreach (array('gender','firstname','lastname','email','phone','mobile') as $name)
            $str.=$this->$name;
        return sha1($str);
    }
    
    function save() {
        $this->encryptPassword();
        parent::insert();        
    }
    
    function delete() {
        $this->deleteAvatar();
        parent::delete();
    }
    
    function isConnected()
    {
        return ($this->is_connected=='YES');
    }
    
    function toJson($fields=array()){
        $item_toJson = array();
        foreach ($fields as $field)
            $item_toJson[$field] = $this->$field;
        return json_encode($item_toJson);
    }
    
    function getExpirationAge()
    {
        //experation age for cookie
        return 15 * 24 * 3600;  
    }
    
    function createFromData($data)
    {
        foreach ($data as $key=>$value)
            $this->$key = $value;
        return $this;
    }
    
    function toArrayForChat()
    {
        $values = array();
        foreach (array("id","username","lastname","firstname","gender") as $field=>$value)
        {
            $values[$value]=$this->$value;
        }
        $values['is_connected'] = $this->isConnected();
        return $values;
    }
    
    static function getUsersConnected()
    {        
        $collection = array();
        $cnx = self::getDatabase()->prepare('SELECT * FROM ' . self::table.
                                            ' ORDER BY id asc');
        $cnx->execute();
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $object = new self();
            $object->fillFromData($data);
            $collection[] = $object->toArrayForChat();
        }
        $cnx->closeCursor();
        return $collection;
    }
}