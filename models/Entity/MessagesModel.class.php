<?php

class MessagesModel extends Orm
{
    protected $id,$source_user_id,$file,$message,$is_seen,$seen_at=null,$_source_user_id=null;
    protected $table="t_chat_messages";   
    const table="t_chat_messages";   
    
    //'source_user_id'=>'User';
    
    function __construct($parameters=null) {
        $this->getDefaults();
    }
    
    function getId(){
        return $this->id;
    }
    
    function getSourceUserId() {
        return $this->source_user_id;
    }

    function getMessage() {
        return $this->message;
    }

    function getIsSeen() {
        return $this->is_seen;
    }
    
    function getSeenAt() {
        return $this->seen_at;
    }
    
    function setId($id){
        $this->id = $id;
    }
    
    function setSourceUserId($source_user_id) {
        $this->source_user_id = $source_user_id;
    }

    function setMessage($message) {
        $this->message = $message;
    }

    function setSeenAt($seen_at) {
        $this->seen_at = $seen_at;
    }
    
    function getTable()
    {
        return $this->table;
    }

    protected function getDefaults()
    {
        $this->is_seen=isset($this->is_seen)?$this->is_seen:"NO";              
        $this->seen_at=isset($this->seen_at)?$this->seen_at:date("Y-m-d H:i:s");
        $this->created_at=isset($this->created_at)?$this->created_at:date("Y-m-d H:i:s");
    }
        
    /* File Methods */
    function getDirectory(){
        return "/data/users/chat/".$this->id;
    }
    
    public function hasFile()
    {
        return (boolean)$this->file;
    }
    
    public function deleteFile()
    {
        //
        return $this;
    }       
    
    static function getName($name)
    {      
        return preg_replace('/[^abcdefghijklmnopqrstuvwxyz0123456789\.\-]/i', '-', str_replace(" ","_",$name));
    } 
            
    function setFile($file){
        //
        return $this;
    }
    
    function saveFile(){
        //
        return $this;
    }
    /* END */
    
    public function getSourceUser()
    {
        if($this->_source_user_id===null)
        {
            try {
                $user = new UserModel();
                $cnx = self::getDatabase()->prepare('SELECT * FROM ' . UserModel::table . ' WHERE id=:id');
                $cnx->bindParam(':id', $this->source_user_id, PDO::PARAM_STR);
                $cnx->execute();
                while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
                    $user->fillFromData($data);
                    $this->_source_user_id = $user;
                }
                $cnx->closeCursor();
            } 
            catch (PDOException $ex) {
                throw new Exception($ex->getMessage());
            }
        }
        return $this->_source_user_id;        
    } 
    
    public static function getAllMessagesWithUsers()
    {
        $collection = array();
        $cnx = self::getDatabase()->prepare('SELECT * FROM ' . self::table.' ORDER BY id asc');
        $cnx->execute();
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $message = new self();
            $message->fillFromData($data);
            $message->getSourceUser();
            $collection[] = $message;
        }
        $cnx->closeCursor();
        return $collection;
    }
            
    function setIsSeen($value,$user){

        if($this->source_user_id!=$user->id)
        {
            if($this->is_seen=="YES")
                return $this;
            $value = $value=="YES"?"YES":"NO";
            $this->is_seen=$value;
            $this->seen_at=date("Y-m-d H:i:s");
            $this->update(array('is_seen'));
        }
        return $this;
    }
    
    function toArrayForChat(){
        
        $values = array();
        foreach (array("id","message","is_seen","source_user_id","seen_at") as $field=>$value)
        {
            $values[$value]=$this->$value;
        }
        $values['user'] = $this->getSourceUser()->getUsername();
        return $values;
    }
    
    function createFromData($data=array())
    {
        foreach ($data as $key=>$value)
        {
            $this->$key = $value;
        }
        return $this;
    }
    
    static function getLastMessages($last_id=0)
    {
        if(!$last_id)
        {
            $all_messages = self::getAllMessagesWithUsersForChat();
            if(count($all_messages))
                return $all_messages;
            return array();
        }
        
        $collection = array();
        $cnx = self::getDatabase()->prepare('SELECT * FROM ' . self::table.
                                            ' WHERE id>:id'.
                                            ' ORDER BY id asc');
        $cnx->execute(array(':id'=>$last_id));
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $object = new self();
            $object->fillFromData($data);
            $object->getSourceUser();
            $collection[] = $object->toArrayForChat();
        }
        $cnx->closeCursor();
        return $collection;
    }

    public static function getAllMessagesWithUsersForChat()
    {
        $collection = array();
        $cnx = self::getDatabase()->prepare('SELECT * FROM ' . self::table.' ORDER BY id asc');
        $cnx->execute();
        while ($data = $cnx->fetch(PDO::FETCH_ASSOC)) {
            $message = new self();
            $message->fillFromData($data);
            $message->getSourceUser();
            $collection[] = $message->toArrayForChat();
        }
        $cnx->closeCursor();
        return $collection;
    }
}