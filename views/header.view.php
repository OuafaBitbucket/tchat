<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tchat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script>
        function loading(show) {
            if (show) {
                $('button').blur();
                $('.loading').fadeIn();
            }
            else {
                $('button').blur();
                $('.loading').fadeOut();
            }
        }
    </script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">T'chat</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->url;?>">Home</a>
            </li>
        </ul>
        <ul class="navbar-nav pull-right">
            <?php if (!Session::hasUser()) { ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->url;?>user/login">Sign In</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= $this->url;?>user/register">Sign Up</a>
                </li>
            <?php } else { ?>
                <li class="nav-item">
                    <a class="nav-link btn btn-danger text-white" href="<?= $this->url;?>user/logout">logout</a>
                </li>
            <?php } ?>
        </ul>
    </div>
</nav>
<section id="target" class="container">