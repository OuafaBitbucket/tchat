<div id="messages"><?= $this->messages;?></div>
<section class="col-md-6 offset-md-3">
    <div class="loading" style="display: none;position: absolute;background: white;width: 100%;height: 100%;z-index: 200000;text-align: center;"><img src="https://loading.io/spinners/coolors/lg.palette-rotating-ring-loader.gif"><br/>Chargement...</div>
    <h1 class="text-center">Sign Up</h1>
    <div class="form" >
        <div class="form-group">
            <label for="firstname">Firstname</label>
            <input type="text" name="firstname" class="form-control Input" id="firstname" placeholder="firstname">
        </div>
        <div class="form-group">
            <label for="lastname">Lastname</label>
            <input type="text" name="lastname" class="form-control Input " id="lastname" placeholder="Lastname">
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" class="form-control Input" id="email" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control Input" id="username" placeholder="Username">
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" class="form-control Input" id="password" placeholder="Password">
        </div>
        <button type="button" class="btn btn-primary pull-right RegisterClick">Register</button>
    </div>
</section>
<script>

    $(document).ready(function() {
         $('.RegisterClick').click(function() {
            var params = { };
            $(".Input").each(function() { params[$(this).attr('name')]=$(this).val() });
            
            $.ajax({
                url:"<?= $this->url;?>user/register",
                method: "post",
                data: params,
                dataType:'json',
            }).done(function(response){
                if(response.error)
                {
                    $('#messages').html(response.error);
                }
                else if(response.success)
                    window.location.href="<?= $this->url;?>"+response.link;
            });
        });
    });
//    $('form').submit(function(e){
//        e.preventDefault();
//        loading(true);
//        $.post($(this).attr('action'), $(this).serialize()).then(function(e){
//            console.log(e);
//            if (e==='false'){
//                alert("Inscription failed!!");
//            }
//            else {
//                document.location.href = '<?php //echo $URL ?>'
//            }
//            loading(false);
//        });
//    })
</script>