<style>
    .msgcontainer {
        background-color: #f8f9fa;
        height: 500px;
        padding: 10px;
        overflow-y: auto
    }

    .msgbox {
        padding: 10px;
        background: #fdfdfd;
        border-radius: 5px;
        margin-bottom: 5px;
    }
    .msgbox:first-letter {
        text-transform: capitalize;
    }

    .usrOnline {
        display: block;
        padding: 4px;
        background: #f8f9fa;
        width: 100%;
        border-radius: 5px;
        margin-bottom: 5px;
    }

    .usrOnlineDot {
        background: rgb(66, 183, 42);
        border-radius: 50%;
        display: inline-block;
        height: 9px;
        margin-left: 4px;
        width: 9px;
        box-shadow: 1px 1px 1px #b8dab2;
        margin-right: 10px;
    }
    
    .usrOfflineDot {
        background: #F8F9FA;
        border-radius: 50%;
        display: inline-block;
        height: 9px;
        margin-left: 4px;
        width: 9px;
        box-shadow: 1px 1px 1px #b8dab2;
        margin-right: 10px;
    }
    #btnSend{
        margin-bottom: 10px;
    }
</style>
<?php // $Username = Session::hasUser() ? Session::getUser()->getFullName() : '' ?>
<h1>Bonjour, <?php echo Session::getUser()->getFullName() ?></h1>
<section class="row">
    <article class="col-md-8">
        <div class="msgcontainer" id="msgContainer">
            <?php foreach ($this->messages as $msg) { ?>
                <div class="msgbox" data-id="<?php echo $msg->getId() ?>"><b><?php echo $msg->getSourceUser()->getUsername() ?> : </b><?php echo $msg->getMessage() ?></div>
            <?php } ?>
        </div>
        <div style="margin-top:10px;background-color:#fefefe">
            <div action="sendMessage" method="post">
                <textarea name="message" id="txtMsg" style="width: 100%;margin-bottom: 10px" class="form-control Message"
                      placeholder="Votre message"></textarea>
                <button class="btn btn-success" type="button" id="btnSend">Send</button>
            </div>
        </div>
    </article>
    <aside class="col-md-4" id="connectedUsers">
        <?php foreach ($this->listUsers as $user) { ?>
            <div class="usrOnline" data-id="<?php echo $user->getId() ?>"><span data-id="<?php echo $user->getId() ?>" class="status-connection-icon <?php if($user->isConnected()) echo 'usrOnlineDot'; else echo 'usrOfflineDot'; ?> "></span>
                                                          <?php echo $user->getUsername() ?>
                <small data-id="<?php echo $user->getId() ?>" class="text-muted status-connection-text"><i><?php if($user->isConnected()) echo "(Online)"; else echo "(Offline)"; ?></i></small>
            </div>
        <?php } ?>
    </aside>
</section>

<script>
    $(document).ready(function() {
        $('#btnSend').click(function() {
            var params = { source_user_id: <?php echo Session::getUser()->getId() ?> };
            $(".Message").each(function() { params[$(this).attr('name')]=$(this).val() });
            
            //check if the textarea is empty
            if (params.message === '')
                return false;
            
            $.ajax({
                url:"<?= $this->url;?>messages/sendMessage",
                method: "post",
                data: params,
                dataType:'json',
            }).done(function(response){
                $('#msgContainer').append('<div class="msgbox" data-id="'+response.id+'"><b><?php echo Session::getUser()->getUsername() ?> : </b>' + response.message + '</div>');
                $('#msgContainer').scrollTop(1E10);
                $('#txtMsg').val('');
            });
        });
        
        //update list messages
        window.setInterval(function () {
        //get messages
            $.ajax({
                    url:"<?= $this->url;?>messages/getLastMessages",
                    method: "post",
                    data: { last_message_id: $('.msgbox').last().attr('data-id')},
                    dataType:'json',
                }).done(function(response){
                    $.each(response, function (i, item) {
                        $('#msgContainer').append('<div class="msgbox" data-id="'+item.id+'"><b>'+item.user+' : </b>' + item.message + '</div>');
                    });
                    $('#msgContainer').scrollTop(1E10);
            });
        //get users conected
            $.ajax({
                    url:"<?= $this->url;?>user/getConnectedUsers",
                    method: "post",
                    dataType:'json',
                }).done(function(response){
                    $.each(response, function (i, item) {
                        var class_to_add = (item.is_connected)?'usrOnlineDot':'usrOfflineDot';
                        var class_to_remove = (item.is_connected)?'usrOfflineDot':'usrOnlineDot';
                        var text_to_change = (item.is_connected)?'(Online)':'(Offline)';
                        $('.status-connection-icon[data-id="'+item.id+'"]').removeClass(class_to_remove).addClass(class_to_add);
                        $('.status-connection-text[data-id="'+item.id+'"]').text(text_to_change);
                    });
            });
            }, 3000);
    });
    
</script>