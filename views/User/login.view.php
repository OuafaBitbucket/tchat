<div id="messages"><?= $this->messages;?></div>
<section class="col-md-6 offset-md-3">
    <div class="loading" style="display: none;position: absolute;background: white;width: 100%;height: 100%;z-index: 200000;text-align: center;"><img src="https://loading.io/spinners/coolors/lg.palette-rotating-ring-loader.gif"><br/>Chargement...</div>
    <h1 class="text-center">Sign In !</h1>
    <div class="form">
        <div class="form-group">
            <label for="username">Usrname/email</label>
            <input type="text" name="username" class="form-control Input" id="username" placeholder="Username">
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control Input" id="password" placeholder="Mot de passe">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input CheckBox" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Remember me</label>
        </div>
        <button type="button" class="btn btn-success pull-right LoginClick">Login</button>
    </div>
</section>
<script>
    //ajax
    $(document).ready(function() {
        $('.LoginClick').click(function() {
            var params = { };
            $(".Input").each(function() { params[$(this).attr('name')]=$(this).val() });
            
            $.ajax({
                url:"<?= $this->url;?>user/login",
                method: "post",
                data: params,
                dataType:'json',
            }).done(function(response){
                if(response.error)
                {
                    $('#messages').html(response.error);
                }
                else if(response.success)
                    window.location.href="<?= $this->url;?>"+response.link;
            });
        });
    });
</script>