<?php
require_once 'config/database.php';

$currentURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$currentURL = str_replace(substr(strrchr($currentURL, "/"), 0), '', $currentURL);

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['server'], $_POST['user'], $_POST['password'], $_POST['database'], $_POST['baseurl'])) {
    $query = file_get_contents('database.sql');
    try {
        $cnx = new PDO('mysql:host=' . $_POST['server'] . ';charset=utf8', $_POST['user'], $_POST['password']);
        $cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $cnx->query("CREATE DATABASE IF NOT EXISTS " . $_POST['database']);
        $cnx->query("use " . $_POST['database']);

        $cnx = new PDO("mysql:host=" . $_POST['server'] . ";dbname=" . $_POST['database'], $_POST['user'], $_POST['password']);
        $cnx->query($query);
        header('location: ' . $_POST['baseurl']);
    } catch (\Exception $e) {
        echo $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Installation of Hiit T'chat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body class="container">
<h1 class="text-center">T'chat installation wizard</h1>
<section>
    <div>
        <h2 class="text-center text-muted">Server Informations</h2>
        <div class="col-md-4 offset-md-4">
            <div class="jumbotron" style="padding-top: 28px;padding-bottom: 28px;">
                <form action="" method="post">
                    <div class="form-group">
                        <label>Database server</label>
                        <input type="text" name="server" readonly="readonly" placeholder="serveur" class="form-control" value="<?php echo DB_HOST ?>">
                    </div>
                    <div class="form-group">
                        <label>User</label>
                        <input type="text" name="user" readonly="readonly" placeholder="User" class="form-control" value="<?php echo DB_USER ?>">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="text" name="password" readonly="readonly" placeholder="Password" class="form-control" value="<?php echo DB_PASS ?>">
                    </div>
                    <div class="form-group">
                        <label>Database name</label>
                        <input type="text" name="database" readonly="readonly" placeholder="Database name" class="form-control"
                               value="<?php echo DB_NAME ?>">
                    </div>
                    <div class="form-group">
                        <label>URL de base</label>
                        <input type="text" name="baseurl" placeholder="Database path" class="form-control"
                               value="<?php echo $currentURL ?>">
                    </div>
                    <button class="btn btn-block btn-primary" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
</section>
<p>If you want to change any information go to config/database.php</p>
</body>
</html>