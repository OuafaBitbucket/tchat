**Tchat**
Mini Chat application built with PHP, in which you can communicate in real time with other users in the chat room.
---

## All features

1. Login.
2. Register.
3. Chat with other connected users.
4. See all user with connection status (if a user is connected or not)


---

## Installation

=> /tchat/install : A wizard that helps you to install the database at your local environment using the default informations in the file : config/database.php.

---


## Tests

=> /tchat/index : redirect you to either the login page if you are not connected or the chat room.

=> /tchat/user/login : login page.

=> /tchat/user/register : register page.

=> /tchat/user/chatRoom : chating room page.

---

## Upcoming

=> Form Model for the validation of data.

=> Fixe The routing issue.

