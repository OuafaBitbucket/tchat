<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
//Class Session manage the session object
class Session
{
    public static function init()
    {
        //start the session
        @session_start();
    }
    public static function set($key, $value)
    {
        //set a value to save in the session
        $_SESSION[$key] = $value;
    }
    public static function setUser($user)
    {
        //set the user of the session
        $_SESSION['user'] = $user;
    }
    public static function getUser()
    {
        //return the user of the session
        return $_SESSION['user'];
    }
    public static function hasUser()
    {
        //return the user of the session
        return isset($_SESSION['user']);
    }
    public static function get($key){
        //return a value saved in the session
        if (isset($_SESSION[$key]))
            return $_SESSION[$key];
    }
    public static function destroy()
    {
        //end the session
        session_destroy();
    }
}