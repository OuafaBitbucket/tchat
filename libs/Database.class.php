<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
class Database {
    
    private static $_instance = null;
    
    private function __construct(){
        try {
            self::$_instance = new PDO(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }
        catch (PDOException $ex)
        {
            throw new Exception("Erreur de connexion ".$ex->getMessage());
        }        
    }
    
    public static function getInstance() {
 
        if(is_null(self::$_instance)) {
            new self();  
        }
        return self::$_instance;
    }
}
