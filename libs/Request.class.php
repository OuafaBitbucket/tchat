<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
class Request {
    
    protected $methode="",$post=array(),$get=array();
    
    function __construct(){
        //get methode
        $this->methode = $_SERVER['REQUEST_METHOD'];
        //put the params in the variables
        $this->post=$_POST;
        $this->get=$_GET;
    }
    
    public function getMethod() {
        return $this->methode;
    }
    
    public function isMethodePost()
    {
        return ($this->methode=== 'POST')?TRUE:FALSE;
    }
    
    function getPostParameters()
    {
        //all post params
        return $this->post;
    }
    
    function getGetParameters()
    {
        //all get params
        return $this->get;
    }
    
    function getPostParameter($name)
    {
        if(isset($this->post[$name]))
            return $this->post[$name];
        return null;
    }
    
    function getGetParameter($name)
    {
        if(isset($this->get[$name]))
            return $this->get[$name];
        return null;
    }
    
    function getRequestUri()
    {
        return $_SERVER['REQUEST_URI'];
    }
    
    function getHost()
    {
        return $_SERVER['HTTP_HOST'];
    }
    
    function getURI()
    {
        if(strpos($this->getRequestUri(), '/tchat/')!==false)
            return '/tchat/';
        else {
            return '/';      
        }
        
    }
}
