<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
class Controller {
    
    protected $path="",$model=null;

    const prifex_model = "Model";
    const path_model = "models/";
    
    function __construct(){
        //initialise view class
        $this->view = new Views();
    }
    
    public function loadModel($name) {
        $this->path = self::path_model.$name.self::prifex_model.'.class.php';
        
        if (file_exists($this->path)) {
            require_once $this->path;
            
            $modelName = $name.self::prifex_model;
            $this->model = new $modelName();
        }
    }
    
}
