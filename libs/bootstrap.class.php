<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
class Bootstrap {
    
    protected $path="",$controller="",$method="";
    const path_controller = "controllers/";
    const prifex_controller = "Controller";
    const error_controller = "controllers/error";
    const path_static = "/tchat/";
    const default_controller = "index";
    
    function __construct(){
        //construct and clean the path
        $this->path= $_SERVER["REQUEST_URI"];

        if(strpos($this->path, self::path_static)!==false)
            $this->path = str_replace (self::path_static, '', $this->path);

        $this->path = rtrim($this->path);
        $this->path = explode('/',rtrim($this->path));

        if($this->path[0] === '')
            $this->path[0] = self::default_controller;
        $this->controller = $this->path[0];
        if(isset($this->path[1]))
            $this->method = $this->path[1];
        $this->loadController();
    }
    
    protected function loadController()
    {
        //loade the controller if exist
        $this->controller = self::path_controller.$this->controller.self::prifex_controller.".class.php";
        
        if (file_exists($this->controller)) {
            require_once $this->controller;
        } 
        else {
            require_once self::error_controller.self::prifex_controller.".class.php";
            $controller = new errorController();
            return false;
        }
        //construct and call the controller
        $name = $this->path[0].self::prifex_controller;
        $controller = new $name;
        //load the model of the controller
        $controller->loadModel($this->path[0]);
        
        if(isset($this->path[2])){
            $controller->{$this->path[1]}($this->path[2]);
        }
        else{
            if(isset($this->path[1])){
                $controller->{$this->path[1]}();
            }
        }
    }
}
