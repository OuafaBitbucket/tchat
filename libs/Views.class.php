<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of bootstrap
 *
 * @author TOSHIBA
 */
class Views {
    
    protected $view="";
    const path_view = "views/";
    const prifex_view = "View";
    const path_header = "views/header.view.php";
    const path_footer = "views/footer.view.php";
    
    function __construct(){ 
        
    }
    
    public function render($name){
        //load header
        require_once self::path_header;
        //load the view
        require_once self::path_view.$name.".view.php";
        //load footer view
        require_once self::path_footer;
    }
}
